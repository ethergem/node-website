const Web3 = require("web3");
var mysql = require('mysql');
var express = require('express');
var session = require('express-session');
var bodyParser = require('body-parser');
var path = require('path');
const bcrypt = require('bcrypt');

const nodelist = require('./nodes.json');
const summary = require('./summary.json');
const settings = require("./configs/settings.json");
const app = express();
var net = require('net');
//local
var web3 = new Web3(new Web3.providers.IpcProvider(settings.web3IPC, net));

var helmet = require('helmet')

var connection = mysql.createConnection({
  host: settings.mysqlip,
  user: settings.mysqluser,
  password: settings.mysqlpass,
  database: settings.mysqldb
});

app.use(session({
	secret: settings.sessionsecret,
	resave: true,
	saveUninitialized: true
}));

app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());

app.use(helmet())
app.set('view engine', 'pug');
app.use(express.static(__dirname + '/public'));

const tagline = "egem.io"

app.get('/', (req, res) => {
  res.render('content/index', {
    title: 'Home',
    tagline,
    summary: summary.settings,
    nodes: nodelist.profiles
  });
});

app.get('/recent', (req, res) => {
  res.render('content/recent', {
    title: 'Recent Blocks',
    tagline
  });
});

app.get('/toc', (req, res) => {
  res.render('content/terms', {
    title: 'ToC Info',
    tagline
  });
});

app.get('/team', (req, res) => {
  res.render('content/team', {
    title: 'Team Page',
    tagline
  });
});

app.get('/community', (req, res) => {
  res.render('content/community', {
    title: 'Community Content',
    tagline
  });
});

app.get('/roadmap', (req, res) => {
  res.render('content/roadmap', {
    title: 'Roadmap & Milestones',
    tagline
  });
});

app.get('/pools', (req, res) => {
  res.render('content/pools', {
    title: 'Pools',
    tagline
  });
});

app.get('/partners', (req, res) => {
  res.render('content/partners', {
    title: 'Partners',
    tagline
  });
});

app.get('/mobile', (req, res) => {
  res.render('content/mobile', {
    title: 'Mobile UX/UI',
    tagline
  });
});

// app.get('/desktop', (req, res) => {
//   res.render('content/desktop', {
//     title: 'Desktop UX/UI',
//     tagline
//   });
// });

app.get('/whitepaper', (req, res) => {
  res.render('content/whitepaper', {
    title: 'Whitepaper',
    tagline
  });
});

app.get('/nodelist', (req, res) => {
  res.render('content/nodelist', {
    title: 'Quarrynode List',
    tagline,
    summary: summary.settings,
    nodes: nodelist.profiles
  });
});

app.get('/stats', (req, res) => {
  res.render('content/stats', {
    title: 'Stats',
    tagline
  });
});

app.get('/blocks', (req, res) => {
  res.render('content/blocks', {
    title: 'Blocks',
    tagline
  });
});

app.get('/tokens', (req, res) => {
  res.render('content/tokens', {
    title: 'Tokens',
    tagline
  });
});

app.get('/contracts', (req, res) => {
  res.render('content/contracts', {
    title: 'Contracts',
    tagline
  });
});

app.get('/accounts', (req, res) => {
  res.render('content/accounts', {
    title: 'Account Query',
    tagline
  });
});

app.get('/txs', (req, res) => {
  res.render('content/txs', {
    title: 'Transactions',
    tagline
  });
});

app.get('/nodeprofile', (req, res) => {
  const node = nodelist.profiles.find(p => p.address == req.query.address);
  res.render('content/nodeprofile', {
    title: `Node Profile`,
    tagline,
    node,
  });
});

app.get('/about', (req, res) => {
  res.render('content/about', {
    title: 'About Us',
    tagline,
    summary: summary.settings,
    nodes: nodelist.profiles
  });
});

app.get('/faq', (req, res) => {
  res.render('content/faq', {
    title: 'FAQ Page',
    tagline
  });
});

app.get('/bugs', (req, res) => {
  res.render('content/bugs', {
    title: 'Bug & Bounty Page',
    tagline
  });
});

// app.get('/register', function(req, res) {
//   res.render('content/register', {
//     title: 'User Registration',
//     tagline
//   });
// });

// app.get('/login', function(req, res) {
//   if (req.session.loggedin) {
//     return res.redirect('/me');
//   } else {
//     res.render('content/login', {
//       title: 'User login',
//       tagline
//     });
//   }
// });
//
// app.get('/logout', function(req, res, next) {
//   if (req.session) {
//     // delete session object
//     req.session.destroy(function(err) {
//       if(err) {
//         return next(err);
//       } else {
//         return res.redirect('/');
//       }
//     });
//   }
// });

// post commands

// trace tx
// app.post('/trace', function(req, res) {
//   var tx = req.body.transaction;
//   async function traceTx(tx) {
//
//   }
//   var cBlock = await web3.eth.getBlock("latest", function(error, result){
//      if(!error){
//        document.getElementById("block-height").innerHTML = `<a href='blocks?=`+result.number+`'>`+result.number+`</a>`;
//      } else {
//        console.error(error);
//      }
//   })
// });

// // auth user
// app.post('/auth', function(req, res) {
//   let checkedValue = req.body['checked'];
//   var password = req.body.password;
//   var address = req.body.egemaddress;
//   var username = req.body.username;
//   var simplecap = req.boby.amihuman;
//   var addyCheck = web3.utils.isAddress(address);
//   // if (simmplecap != settings.secretCap) {
//   //   return res.redirect('/login');
//   // }
//   console.log(addyCheck)
//   if (addyCheck != true) {
//     return res.redirect('/login');
//   }
//   if(checkedValue) { // Runs if the box is not undefined
//     if (password && address) {
//       connection.query('SELECT * FROM userdata WHERE address = ?', [address], function(error, results, fields) {
//         try {
//           let hash = results[0]['password'];
//           bcrypt.compare(password, hash, function(err, result) {
//             if(result) {
//               req.session.loggedin = true;
//               req.session.username = address;
//               req.session.address = results[0]['address'];
//               req.session.idnum = results[0]['id'];
//               req.session.balance = results[0]['balance'];
//               req.session.credits = results[0]['credits'];
//               req.session.paylimit = results[0]['paylimit'];
//               req.session.notify = results[0]['notify'];
//               req.session.autopay = results[0]['autopay'];
//               return res.redirect('/me');
//             } else {
//               return res.redirect('/login');
//             }
//             res.end();
//           });
//         } catch (e) {
//           console.log(e)
//           return res.redirect('/login');
//         }
//       });
//     } else {
//       return res.redirect('/login');
//       res.end();
//     }
//   } else {
//     if (username && password && address) {
//       connection.query('SELECT * FROM userdata WHERE userId = ? AND address = ?', [username, address], function(error, results, fields) {
//         try {
//           let hash = results[0]['password'];
//           bcrypt.compare(password, hash, function(err, result) {
//             if(result) {
//               req.session.loggedin = true;
//               req.session.username = username;
//               req.session.address = results[0]['address'];
//               req.session.idnum = results[0]['id'];
//               req.session.balance = results[0]['balance'];
//               req.session.credits = results[0]['credits'];
//               req.session.paylimit = results[0]['paylimit'];
//               req.session.notify = results[0]['notify'];
//               req.session.autopay = results[0]['autopay'];
//               return res.redirect('/me');
//             } else {
//               return res.redirect('/login');
//             }
//             res.end();
//           });
//         } catch (e) {
//           console.log(e)
//           return res.redirect('/login');
//         }
//       });
//     } else {
//       return res.redirect('/login');
//       res.end();
//     }
//   }
// });
//
//
// // set user paylimit
// app.post('/setpay', function(req, res) {
//   if (req.session.loggedin) {
//     console.log(req.session.username)
//     var idnum = req.body.idnum;
//     var pay = req.body.paylimit;
//     if (req.session.idnum != idnum) {
//       return res.redirect('/login')
//     }
//     if (pay && idnum) {
//       console.log(pay)
//       console.log(idnum)
//       connection.query(`UPDATE userdata SET paylimit = ? WHERE id = ?`, [pay, idnum]);
//       connection.query('SELECT * FROM userdata WHERE id = ?', [idnum], function(error, results, fields) {
//         try {
//           req.session.loggedin = true;
//           req.session.address = results[0]['address'];
//           req.session.username = results[0]['userId'];
//           req.session.idnum = results[0]['id'];
//           req.session.balance = results[0]['balance'];
//           req.session.credits = results[0]['credits'];
//           req.session.paylimit = results[0]['paylimit'];
//           req.session.notify = results[0]['notify'];
//           req.session.autopay = results[0]['autopay'];
//           return res.redirect('/me');
//         } catch (e) {
//           console.log(e)
//           return res.redirect('/logout');
//         }
//       });
//   	} else {
//   		res.end();
//   	}
//   } else {
//     res.send('Please login to use this function!');
//   }
// });
//
// // reg user
// app.post('/reg', function(req, res) {
//   // if (req.session.loggedin) {
//   //   console.log(req.session.username)
//   //   var username = req.body.username;
//   //   var pay = req.body.paylimit;
//   //   if (req.session.username != username) {
//   //     return res.redirect('/login')
//   //   }
//   //   if (pay && username) {
//   //     console.log(pay)
//   //     console.log(username)
//   //     connection.query(`UPDATE userdata SET paylimit = ? WHERE userId = ?`, [pay, username]);
//   //     res.send('Pay has been updated to: '+pay);
//   // 	} else {
//   // 		res.send('Please enter Amount & DiscordId!');
//   // 		res.end();
//   // 	}
//   // } else {
//   //   res.send('Please login to use this function!');
//   // }
// });
//
// // set notufy
// app.post('/notify', function(req, res) {
//   if (req.session.loggedin) {
//     var idnum = req.body.idnum;
//
//     if (req.session.idnum != idnum) {
//       return res.redirect('/login')
//     }
//     if (idnum) {
//       connection.query("SELECT notify, id FROM userdata WHERE id = ?", idnum, function (err, result) {
//         if (err) return message.reply("No Results.");
//         try {
//           let parsed = JSON.stringify(result);
//           let obj = JSON.parse(parsed);
//           let author = idnum;
//           let authorCheck = obj[0]["id"];
//           let autoNotify = obj[0]["notify"];
//           if (authorCheck == author) {
//             if (author == authorCheck) {
//               if (autoNotify == "1") {
//                 let response = "0";
//                 connection.query(`UPDATE userdata SET notify =? WHERE id = ?`, [response,author]);
//                 connection.query('SELECT * FROM userdata WHERE id = ?', [author], function(error, results, fields) {
//                   try {
//                     req.session.loggedin = true;
//                     req.session.address = results[0]['address'];
//                     req.session.username = results[0]['userId'];
//                     req.session.idnum = results[0]['id'];
//                     req.session.balance = results[0]['balance'];
//                     req.session.credits = results[0]['credits'];
//                     req.session.paylimit = results[0]['paylimit'];
//                     req.session.notify = results[0]['notify'];
//                     req.session.autopay = results[0]['autopay'];
//                     return res.redirect('/me');
//                   } catch (e) {
//                     console.log(e)
//                     return res.redirect('/logout');
//                   }
//                 });
//               }
//               if (autoNotify == "0") {
//                 let response = "1";
//                 connection.query(`UPDATE userdata SET notify =? WHERE id = ?`, [response,author]);
//                 connection.query('SELECT * FROM userdata WHERE id = ?', [author], function(error, results, fields) {
//                   try {
//                     req.session.loggedin = true;
//                     req.session.address = results[0]['address'];
//                     req.session.username = results[0]['userId'];
//                     req.session.idnum = results[0]['id'];
//                     req.session.balance = results[0]['balance'];
//                     req.session.credits = results[0]['credits'];
//                     req.session.paylimit = results[0]['paylimit'];
//                     req.session.notify = results[0]['notify'];
//                     req.session.autopay = results[0]['autopay'];
//                     return res.redirect('/me');
//                   } catch (e) {
//                     console.log(e)
//                     return res.redirect('/logout');
//                   }
//                 });
//               }
//               //console.log(obj);
//             }
//           }
//         }catch(e){
//           console.log("ERROR ::",e)
//           res.send("There was an error.");
//         }
//       })
//   	} else {
//   		res.send('Please enter DiscordId!');
//   		res.end();
//   	}
//   } else {
//     res.send('Please login to use this function!');
//   }
// });
//
// // set autopay
// app.post('/autopay', function(req, res) {
//   if (req.session.loggedin) {
//     var idnum = req.body.idnum;
//
//     if (req.session.idnum != idnum) {
//       return res.redirect('/logout')
//     }
//     if (idnum) {
//       connection.query("SELECT autopay, id FROM userdata WHERE id = ?", idnum, function (err, result) {
//         if (err) return message.reply("No Results.");
//         try {
//           let parsed = JSON.stringify(result);
//           let obj = JSON.parse(parsed);
//           let author = idnum;
//           let authorCheck = obj[0]["id"];
//           let autoPay = obj[0]["autopay"];
//           if (authorCheck == author) {
//             if (author == authorCheck) {
//               if (autoPay == "1") {
//                 let response = "0";
//                 connection.query(`UPDATE userdata SET autopay =? WHERE id = ?`, [response,author]);
//                 connection.query('SELECT * FROM userdata WHERE id = ?', [author], function(error, results, fields) {
//                   try {
//                     req.session.loggedin = true;
//                     req.session.address = results[0]['address'];
//                     req.session.username = results[0]['userId'];
//                     req.session.idnum = results[0]['id'];
//                     req.session.balance = results[0]['balance'];
//                     req.session.credits = results[0]['credits'];
//                     req.session.paylimit = results[0]['paylimit'];
//                     req.session.notify = results[0]['notify'];
//                     req.session.autopay = results[0]['autopay'];
//                     return res.redirect('/me');
//                   } catch (e) {
//                     console.log(e)
//                     return res.redirect('/logout');
//                   }
//                 });
//               }
//               if (autoPay == "0") {
//                 let response = "1";
//                 connection.query(`UPDATE userdata SET autopay =? WHERE id = ?`, [response,author]);
//                 connection.query('SELECT * FROM userdata WHERE id = ?', [author], function(error, results, fields) {
//                   try {
//                     req.session.loggedin = true;
//                     req.session.address = results[0]['address'];
//                     req.session.username = results[0]['userId'];
//                     req.session.idnum = results[0]['id'];
//                     req.session.balance = results[0]['balance'];
//                     req.session.credits = results[0]['credits'];
//                     req.session.paylimit = results[0]['paylimit'];
//                     req.session.notify = results[0]['notify'];
//                     req.session.autopay = results[0]['autopay'];
//                     return res.redirect('/me');
//                   } catch (e) {
//                     console.log(e)
//                     return res.redirect('/logout');
//                   }
//                 });
//               }
//               //console.log(obj);
//             }
//           }
//         }catch(e){
//           console.log("ERROR ::",e)
//           res.send("There was an error.");
//         }
//       })
//   	} else {
//   		res.send('Please enter DiscordId!');
//   		res.end();
//   	}
//   } else {
//     res.send('Please login to use this function!');
//   }
// });
//
// // user page
// app.get('/me', (req, res) => {
//   if (req.session.loggedin) {
//     res.render('content/me', {
//       title: 'My Account',
//       tagline,
//       address: req.session.address,
//       idnum: req.session.idnum,
//       me: req.session.username,
//       balance: req.session.balance,
//       credits: req.session.credits,
//       paylimit: req.session.paylimit,
//       notify: req.session.notify,
//       autopay: req.session.autopay
//     });
//   } else {
//     return res.redirect('/login');
//   }
//   res.end();
// });

const server = app.listen(8080, () => {
  console.log(`EtherGem website running → PORT ${server.address().port}`);
});
