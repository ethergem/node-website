var fs = require('fs');
const settings = require("./configs/settings.json");
var request = require('request');

async function fetchEarning(){
  fs.writeFile('nodes.json', '', function(){console.log('Nodes Cleaned')});
  try {
    request('https://api.egem.io/nodelist', function (error, response, body) {
      console.log('error:', error); // Print the error if one occurred
      console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
      console.log('body:', body);
      fs.appendFile('nodes.json', body, function (err) {
        if (err) throw err;
      });
    });
    // var data = JSON.stringify(item);

  } catch (e) {
    console.log(e)
  }
};

async function fetchSummary(){
  fs.writeFile('summary.json', '', function(){console.log('Summary Cleaned')});
  try {
    request('https://api.egem.io/summary', function (error, response, body) {
      console.log('error:', error); // Print the error if one occurred
      console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
      console.log('body:', body);
      fs.appendFile('summary.json', body, function (err) {
        if (err) throw err;
      });
    });
    // var data = JSON.stringify(item);

  } catch (e) {
    console.log(e)
  }
};

var runFetch = function theTasks() {
  fetchEarning();
  fetchSummary();
  console.log("Updated!")
}
setInterval(runFetch,300000);

fetchEarning();
fetchSummary();
