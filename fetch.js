var mysql = require('mysql');
var fs = require('fs');
const settings = require("./configs/settings.json");

var con = mysql.createPool({
  connectionLimit : 250,
  host: settings.mysqlip,
  user: settings.mysqluser,
  password: settings.mysqlpass,
  database: settings.mysqldb
});

// Update Node List: Online/Offline
async function queryNodes(){
  try {
    con.getConnection(function(err, connection) {
        if (err) throw err; // not connected!
          connection.query("SELECT * FROM userdata", function (err, result) {
            if (err) return console.log("No Results.");
            try{
              let parsed = JSON.stringify(result);
              let obj = JSON.parse(parsed);
              var writeIt = [];

              function delay() {
                return new Promise(resolve => setTimeout(resolve, 50));
              }

              function delayLong() {
                return new Promise(resolve => setTimeout(resolve, 15000));
              }

              async function delayedLog(item) {
                // await promise return
                await delay();
                // log after delay
                if (item.earning == "1") {
                  connection.query("SELECT count AS count1 FROM usernodes1 WHERE id =?",[item.id], function (err, result) {
                    if (err) return console.log("No Results.");
                    connection.query("SELECT count AS count2 FROM usernodes2 WHERE id =?",[item.id], function (err, result2) {
                      if (err) return console.log("No Results.");
                      connection.query("SELECT MAX(timestamp) AS lasttx FROM txdatasent WHERE `to` =?",[item.address], function (err, result3) {
                        if (err) return console.log("No Results.");
                        let latest = result3[0]['lasttx'];
                        connection.query("SELECT hash AS hash FROM txdatasent WHERE `timestamp` =?",[latest], function (err, result4) {
                          if (err) return console.log("No Results.");

                          try {
                            var count1 = JSON.stringify(result[0]);
                            var count2 = JSON.stringify(result2[0]).slice(1);
                            var count3 = JSON.stringify(result3[0]);
                            var count4 = JSON.stringify(result4[0]);

                            if (result4[0] == undefined) {
                              let count4 = '""hash":null"'
                              var data = JSON.stringify(item);
                              var count0p = count3.slice(1, -1);
                              var count1p = count1.slice(1, -1);
                              var count2p = count4.slice(1, -1);

                              var itemP = data.slice(0, -1)
                              var dataFinal = itemP+","+count0p+","+count2p+","+count1p+","+count2;
                              fs.appendFile('nodes.json', dataFinal+",", function (err) {
                                if (err) throw err;
                                //console.log('Updated!');
                              });
                            } else {
                              var data = JSON.stringify(item);
                              var count0p = count3.slice(1, -1);
                              var count1p = count1.slice(1, -1);
                              var count2p = count4.slice(1, -1);
                              var itemP = data.slice(0, -1)
                              var dataFinal = itemP+","+count0p+","+count2p+","+count1p+","+count2;
                              fs.appendFile('nodes.json', dataFinal+",", function (err) {
                                if (err) throw err;
                                //console.log('Updated!');
                              });
                            }

                          } catch (e) {
                            console.log(e)
                          } finally {

                          }
                        })

                      })
                    })
                  })

                }
              }

              async function processArray(array) {
                try {
                  await fs.writeFile('nodes.json', '', function(){console.log('Node Data Cleaned')});
                  await delayLong();
                  for (const item of array) {
                    await delayedLog(item);
                  }
                  await delayLong();
                  await fs.readFile('nodes.json', function (err, data) {
                    if (err) throw err;
                    console.log(data);
                    var dataWrite = data.slice(0, -1)
                    var dataFinal2 = `{"profiles":[`+dataWrite+`]}`
                    //console.log(dataFinal2)
                    fs.writeFile('nodes.json', dataFinal2, function (err) {
                      if (err) throw err;
                      console.log('Node Data Replaced!');
                    });
                  });
                } catch (e) {
                  console.log(e)
                }
              }
              processArray(obj);

            }catch(e){
              console.log("ERROR ::",e)
            }
          })
          connection.release();
    })
  } catch (e) {
    console.log(e)
  }

  try {
    con.getConnection(function(err, connection) {
        if (err) throw err; // not connected!
          connection.query("SELECT * FROM usersystems", function (err, result) {
            if (err) return console.log("No Results.");
            try{
              let parsed = JSON.stringify(result);
              let obj = JSON.parse(parsed);

              var data = parsed.slice(1, -1)
              var dataFinal = `{"settings":[`+data+`]}`

              fs.writeFile('nodes.json', '', function(){
                console.log('Summary Cleaned')
                fs.writeFile('summary.json', dataFinal, function (err) {
                  if (err) throw err;
                  console.log('Summary Replaced!');
                });
              });

            }catch(e){
              console.log("ERROR ::",e)
            }
          })
        connection.release();
    })
  } catch (e) {
    console.log(e)
  }

};

var runTask = function theTasks() {
  queryNodes();
}

setInterval(runTask,300000);
//queryNodes();
