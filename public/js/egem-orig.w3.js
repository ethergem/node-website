
let web3Injected = window.web3;

if(typeof web3Injected !== 'undefined'){
  console.log("Forcing EtherGem web3 injection!");
  web3 = new Web3(new Web3.providers.HttpProvider("https://lb.rpc.egem.io"));
} else {
  console.log("EtherGem web3 injected!");
  web3 = new Web3(new Web3.providers.HttpProvider("https://lb.rpc.egem.io"));
}

function getNet() {
  web3.version.getNetwork((err, netId) => {
    switch (netId) {
      case "1987":
        console.log('This is the EtherGem main network.')
        break
      default:
        console.log('This is an unknown network.')
    }
  })
}

// Currency & Number Formatting
var l10nEN = new Intl.NumberFormat("en-US")
var l10nUSD = new Intl.NumberFormat("en-US", { style: "currency", currency: "USD" })
var l10nGBP = new Intl.NumberFormat("en-GB", { style: "currency", currency: "GBP" })
var l10nEUR = new Intl.NumberFormat("de-DE", { style: "currency", currency: "EUR" })

async function getBlock() {
  var cBlock = await web3.eth.getBlock("latest", function(error, result){
     if(!error){
       document.getElementById("block-height").innerHTML = `<a href='blocks?=`+result.number+`'>`+result.number+`</a>`;
     } else {
       console.error(error);
     }
  })
}

async function getBlocks() {
  var cBlock = await web3.eth.getBlock("latest", function(error, result){
     if(!error){
       for (var i = 0; i < 5; i++) {
           var number = result.number;
           web3.eth.getBlock(number - i - 1, function(error, theblock){
              if(!error){
                let time = theblock.timestamp;
                let size = theblock.size;
                let tx = theblock.transactions;
                let xData = web3.toUtf8(theblock.extraData);
                let dt = new Date(time*1000);
                document.getElementById("blocklist").innerHTML += "<hr><p><i class=\"fal fa-link\"> Number: <a href='blocks?="+theblock.number+"'>"+l10nEN.format(theblock.number)+"</a> <br><i class=\"fal fa-clock\"></i> Timestamp: <br>"+dt+"<br><i class=\"fal fa-hashtag\"></i>  Block Hash: <span id='hash' style=''>"+theblock.hash+"</span> <br><i class=\"fal fa-hammer\"></i> Found By: <a href='accounts?="+theblock.miner+"'>"+theblock.miner+"</a> <br><i class=\"fal fa-gas-pump\"></i> Gas Used: "+theblock.gasUsed+"<br><i class=\"fal fa-puzzle-piece\"> Difficulty: "+theblock.difficulty+" <br><i class=\"fal fa-box\"></i> Size: "+theblock.size+" <br><i class=\"fal fa-balance-scale\"></i> Tx Count: "+tx.length+"<br><i class=\"fal fa-puzzle-piece\"></i> Extra Data: "+xData+"</p>";
              } else {
                console.error(error);
              }
           })
       }
     } else {
       console.error(error);
     }
  })
}

function clearBlocks() {
  document.getElementById("blocklist").innerHTML = "";
}

async function getBalance() {
  let hotwallet = "0xe3696bd9aeec0229bb9c98c9cc7220ce37852887";
  let coldmultisig = "0x87045b7badac9c2da19f5b0ee2bcea943a786644";

  var wallet = await web3.eth.getBalance(hotwallet,function(error, result){
     if(!error){
       document.getElementById("wallet").innerHTML = result/Math.pow(10,18);
     } else {
       console.error(error);
     }
  })
  var multisig = await web3.eth.getBalance(coldmultisig ,function(error, result){
     if(!error){
       document.getElementById("multisig").innerHTML = result/Math.pow(10,18);
     } else {
       console.error(error);
     }
  })
}

// async function getBalance2(){
//   let wallet="0x281bc252c7a61dab43793bebd6f43a3ca51ec292";
//   var get1 = web3.fromWei(web3.eth.getBalance(wallet),"ether")
// document.getElementById("btsEGEM").innerHTML=(get1).toFixed(8)
// }

// Get balance of the address from bar
async function qBalance() {
    var x = document.querySelector('[name="addressInput"]').value;
    window.location.replace('/accounts?='+x);
}

async function qBalance1() {
    var url = window.location.href;
    var params = url.split('?=');
    var x = params[1];

    if (x == null) {
      return;
    }
    var wallet = await web3.eth.getBalance(x,function(error, result){
       if(!error){
         document.getElementById('address').innerText = `Address: `+x;
         document.getElementById("balance").innerHTML = result/Math.pow(10,18)+` EGEM`;
       } else {
         console.error(error);
       }
    })
    var sfrxwallet = await web3.eth.getBalance(x, 1530000, function(error, result){
       if(!error){
         document.getElementById("sfrxbalance").innerHTML = (result/Math.pow(10,18) * 2)+" SFRX";
       } else {
         console.error(error);
       }
    })
}

// Look up a transaction
function qTx() {
    var x = document.querySelector('[name="txInput"]').value;
    window.location.replace('/txs?='+x);
}

function qTx1() {
    var url = window.location.href;
    var params = url.split('?=');
    var x = params[1];

    if (x == null) {
      return;
    }
    web3.eth.getTransaction(x, function(error, result){
      if(!error && result != null){
        var txObj = result;
        data = JSON.stringify(txObj);
        obj = JSON.parse(data);
        document.getElementById("txfrom").innerHTML = `From: <a href='accounts?=`+obj.from+`'>`+obj.from+`</a>`;
        document.getElementById("txto").innerHTML = `To: <a href='accounts?=`+obj.to+`'>`+obj.to+`</a>`;
        document.getElementById("blockhash").innerHTML =  `Hash: `+obj.blockHash;
        document.getElementById("gas").innerHTML = `Gas: `+obj.gas;
        document.getElementById("number").innerHTML = `Block Mined: <a href='blocks?=`+obj.blockNumber+`'>`+obj.blockNumber+`</a>`;
        document.getElementById("value").innerHTML = `Value: `+web3.fromWei(obj.value, "ether")+ " EGEM";
        document.getElementById("input").innerHTML = `Input: `+obj.input;
      } else {
        document.getElementById("notx").innerHTML = `No transaction found.`;
        console.log(error);
      }
    })
}

// Block search
async function qBlock() {
    var x = document.querySelector('[name="blockInput"]').value;
    window.location.replace('/blocks?='+x);
}

async function qBlock1() {
    var url = window.location.href;
    var params = url.split('?=');
    var x = params[1];
    var input = x;
    if (x == null) {
      return;
    }
    var cBlock = await web3.eth.getBlock(input, function(error, result){
       if(!error && result != null){
         web3.eth.getBlockTransactionCount(input, function(error, result){
           if(!error){
             document.getElementById("btransactions").innerHTML = `Transactions: `+result;
           } else {
             document.getElementById("btransactions").innerHTML = "No data"
           }
         })
         web3.eth.getBlockUncleCount(input, function(error, result){
           if(!error){
             document.getElementById("buncles").innerHTML = `Uncles: `+result;
           } else {
             document.getElementById("buncles").innerHTML = "No data"
           }
         })
         txObj = result;
         data = JSON.stringify(txObj);
         obj = JSON.parse(data);
         var dateTimeString = obj.timestamp;
         var dt = new Date(dateTimeString*1000);
         document.getElementById("bNum").innerHTML = `Block Number: `+obj.number;
         document.getElementById("hash").innerHTML = `Hash: `+obj.hash;
         document.getElementById("pHash").innerHTML = `Parent Hash: `+obj.parentHash;
         document.getElementById("miner").innerHTML = `Miner: <a href='accounts?=`+obj.miner+`'>`+obj.miner+`</a>`;
         document.getElementById("nonce").innerHTML = `Nonce: `+obj.nonce;
         document.getElementById("size").innerHTML = `Size: `+obj.size;
         document.getElementById("ltransactions").innerHTML = `Transactions: `+obj.transactions;
         document.getElementById("timestamp").innerHTML = dt;
         document.getElementById("extraData").innerHTML = web3.toUtf8(obj.extraData);
         var i, x = "";
         for (i in obj.transactions) {
             x += `<a href='txs?=`+obj.transactions[i]+`'>`+obj.transactions[i]+`</a></br>`;
         }
         document.getElementById("ltransactions").innerHTML = `Transaction List: `+x;
       } else {
         document.getElementById("noblock").innerHTML = `No block found.`;
         console.log(error);
       }
    })
}

function searchBar() {
  var checkBox = document.getElementById("nodeCheck");
  var str = document.querySelector('[name="mainInput"]').value;
  // regex
  var resultAddress = str.match(/^0x[a-fA-F0-9]{40}$/)
  var resultTx = str.match(/^0x[a-fA-F0-9]{64}$/)
  var resultBlock = str.match(/^[0-9][0-9]*$/)

  if (checkBox.checked == true){
    if (resultAddress != null) {
      window.location.replace('/nodeprofile?address='+str);
    }
  } else {
    if (resultAddress != null) {
      window.location.replace('/accounts?='+str);
    }
    if (resultTx != null) {
      window.location.replace('/txs?='+str);
    }
    if (str == "latest") {
      window.location.replace('/blocks?=latest');
    }
    if (str == "bot") {
      window.location.replace('/accounts?=0xeb0d2004fc6fe660c53dcee40c1d987df691329c');
    }
    if (resultBlock != null) {
      window.location.replace('/blocks?='+str);
    }
  }
}

function searchNodes() {
  var str = document.querySelector('[name="nodeInput"]').value;
  var resultAddress = str.match(/^0x[a-fA-F0-9]{40}$/)
  if (resultAddress != null) {
    window.location.replace('/nodeprofile?address='+str);
  }
}

function searchEnter() {
  // Get the input field
  var input = document.getElementById("searchEnter");

  // Execute a function when the user releases a key on the keyboard
  input.addEventListener("keyup", function(event) {
    // Number 13 is the "Enter" key on the keyboard
    if (event.keyCode === 13) {
      // Cancel the default action, if needed
      event.preventDefault();
      // Trigger the button element with a click
      document.getElementById("searchIt").click();
    }
  });
}

function getCoinInfo() {
    var xhr = new XMLHttpRequest();
    var url = 'https://api.egem.io/stats';
    var currentEra = "";
    xhr.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var coinResponse = JSON.parse(this.responseText);
            var blockHeight = parseFloat(coinResponse['stats'][0]['currentBlock']).toFixed();
            var totalSupply = parseFloat(coinResponse['stats'][0]['currentSupply']).toFixed();
            var marketCap = parseFloat(coinResponse['stats'][0]['currentMcap']).toFixed(4);
            var averageUSD = parseFloat(coinResponse['stats'][0]['avgusd']).toFixed(4);
            if (blockHeight > 15000000) {
              var currentEra = "6";
              var minerR = "0.125";
              var devR = "0.025";
              var nodeR = "0.125"
            } else if (blockHeight > 12500000){
              var nextEra = 15000001;
              var currentEra = "5";
              var minerR = "0.25";
              var devR = "0.05";
              var nodeR = "0.25"
            } else if (blockHeight > 10000000){
              var nextEra = 12500001;
              var currentEra = "4";
              var minerR = "0.5";
              var devR = "0.1";
              var nodeR = "0.25"
            } else if (blockHeight >  7500000){
              var nextEra = 10000001;
              var currentEra = "3";
              var minerR = "1";
              var devR = "0.25";
              var nodeR = "0.25"
            } else if (blockHeight >  5000000){
              var nextEra = 7500001;
              var currentEra = "2";
              var minerR = "2";
              var devR = "0.5";
              var nodeR = "0.5"
            } else if (blockHeight >  2500000){
              var nextEra = 5000001;
              var currentEra = "1";
              var minerR = "4";
              var devR = "0.75";
              var nodeR = "0.5"
            } else {
              var nextEra = 2500001;
              var currentEra = "0";
              var minerR = "8";
              var devR = "1";
              var nodeR = "0.5"
            }
            document.getElementById('current-era').innerHTML = currentEra;
            document.getElementById('miner-reward').innerHTML = minerR;
            document.getElementById('dev-reward').innerHTML = devR;
            document.getElementById('node-reward').innerHTML = nodeR;
            document.getElementById('total-egem-supply').innerHTML = l10nEN.format(totalSupply);
            document.getElementById('market-cap-usd').innerHTML = l10nUSD.format(marketCap);
            document.getElementById('average-usd').innerHTML = "$ "+averageUSD;
            document.getElementById('next-era').innerHTML = l10nEN.format(nextEra);
            document.getElementById('total-egem-supply-two').innerHTML = l10nEN.format(totalSupply);
        }
    };
    xhr.open("GET", url, true);
    xhr.send();

    try {
        // Compliant browsers
        xhr = new XMLHttpRequest();
    }

    catch (e) {
        try {
            // IE7+
            xhr = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            // AJAX is not supported
            alert('AJAX is not supported. Please upgrade your browser!');
        };
    };
};

function getCoinInfo2() {
    var xhr = new XMLHttpRequest();
    var url = 'https://api.egem.io/stats';
    var currentEra = "";
    xhr.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var coinResponse = JSON.parse(this.responseText);
            var lockedEGEM = l10nEN.format((coinResponse['stats'][0]['tierTwoNodes'] * 40000) + (coinResponse['stats'][0]['tierOneNodes'] - coinResponse['stats'][0]['tierTwoNodes'] * 10000));
            var countedEGEM = l10nEN.format(coinResponse['stats'][0]['countedBotCoins']);
            document.getElementById('lockedegem').innerHTML = lockedEGEM;
            document.getElementById('countedegem').innerHTML = countedEGEM;

        }
    };
    xhr.open("GET", url, true);
    xhr.send();

    try {
        // Compliant browsers
        xhr = new XMLHttpRequest();
    }

    catch (e) {
        try {
            // IE7+
            xhr = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            // AJAX is not supported
            alert('AJAX is not supported. Please upgrade your browser!');
        };
    };
};

var a = Math.ceil(Math.random() * 10);
var b = Math.ceil(Math.random() * 10);
var c = a + b
function DrawBotBoot()
{
    document.write("What is "+ a + " + " + b +"? ");
    document.write("<input id='BotBootInput' type='text' maxlength='2' size='2'/>");
}
function ValidBotBoot(){
    var d = document.getElementById('BotBootInput').value;
    if (d == c) return true;
    return false;
}

function doChartSupply() {
  var ctx = document.getElementById('supplyTime');

  var labelData = ["0","5,000","2,100,000","2,500,000","5,000,000","7,500,000","10,000,000","12,500,000","15,000,000"];
  var eraData = [0,0,0,1,2,3,4,5,6];
  var brData = [8,8,8,4,2,1,0.5,0.25,0.125];
  var frData = [1,1,1,0.75,0.5,0.25,0.1,0.05,0.025];
  var nrData = [0,0,0.5,0.5,0.5,0.25,0.25,0.25,0.125];

  var supplyChart = new Chart(ctx, {
    type: 'line',
    data: {
      labels: labelData,
      datasets: [{
          data: eraData,
          label: "Era",
          borderColor: "#3e95cd",
          fill: true
        }, {
          data: brData,
          label: "Block Reward",
          borderColor: "#3cba9f",
          fill: true
        }, {
          data: frData,
          label: "Funding Reward",
          borderColor: "#e8c3b9",
          fill: true
        }, {
          data: nrData,
          label: "Node Reward",
          borderColor: "#c45850",
          fill: true
        }
      ]
    },
    options: {
      title: {
        display: true,
        text: 'Block rewards over time. (Scale is in block height.)'
      }
    }
  });
}

function doChartPrice() {
  var xhr = new XMLHttpRequest();
  var url = 'https://api.egem.io/dailyprice';
  xhr.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {

          var response = JSON.parse(this.responseText);

          var ctx = document.getElementById('priceTime');

          var labelArray = [];

          for (var i = 0; i < response.length; i++) {
            labelArray.push(i);
          }

          var labelData = labelArray;
          var priceData = response;

          var supplyChart = new Chart(ctx, {
            type: 'line',
            data: {
              labels: labelData,
              datasets: [{
                  data: priceData,
                  label: "Daily Price USD",
                  borderColor: "#8269D8",
                  fill: true
                }
              ]
            },
            options: {
              title: {
                display: true,
                text: 'Price data over time.'
              }
            }
          });

      }
  };
  xhr.open("GET", url, true);
  xhr.send();

  try {
      // Compliant browsers
      xhr = new XMLHttpRequest();
  }

  catch (e) {
      try {
          // IE7+
          xhr = new ActiveXObject("Msxml2.XMLHTTP");
      }
      catch (e) {
          // AJAX is not supported
          alert('AJAX is not supported. Please upgrade your browser!');
      };
  };

}

function doChartNodes() {
  var xhr = new XMLHttpRequest();
  var url = 'https://api.egem.io/dailynodes';
  xhr.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {

          var response = JSON.parse(this.responseText);

          var ctx = document.getElementById('nodeTime');

          var labelArray = [];

          for (var i = 0; i < response.length; i++) {
            labelArray.push(i);
          }

          var labelData = labelArray;
          var priceData = response;

          var supplyChart = new Chart(ctx, {
            type: 'line',
            data: {
              labels: labelData,
              datasets: [{
                  data: priceData,
                  label: "Daily Nodes",
                  borderColor: "#D0F7FF",
                  fill: true
                }
              ]
            },
            options: {
              title: {
                display: true,
                text: 'Node data over time.'
              }
            }
          });

      }
  };
  xhr.open("GET", url, true);
  xhr.send();

  try {
      // Compliant browsers
      xhr = new XMLHttpRequest();
  }

  catch (e) {
      try {
          // IE7+
          xhr = new ActiveXObject("Msxml2.XMLHTTP");
      }
      catch (e) {
          // AJAX is not supported
          alert('AJAX is not supported. Please upgrade your browser!');
      };
  };

}

function doChartCredits() {
  var xhr = new XMLHttpRequest();
  var url = 'https://api.egem.io/dailycreds';
  xhr.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {

          var response = JSON.parse(this.responseText);

          var ctx = document.getElementById('credsTime');

          var labelArray = [];

          for (var i = 0; i < response.length; i++) {
            labelArray.push(i);
          }

          var labelData = labelArray;
          var priceData = response;

          var supplyChart = new Chart(ctx, {
            type: 'line',
            data: {
              labels: labelData,
              datasets: [{
                  data: priceData,
                  label: "Daily Tip/Rain/Games Credits",
                  borderColor: "#E4EBFD",
                  fill: true
                }
              ]
            },
            options: {
              title: {
                display: true,
                text: 'Tip/Rain/Games system balance over time.'
              }
            }
          });

      }
  };
  xhr.open("GET", url, true);
  xhr.send();

  try {
      // Compliant browsers
      xhr = new XMLHttpRequest();
  }

  catch (e) {
      try {
          // IE7+
          xhr = new ActiveXObject("Msxml2.XMLHTTP");
      }
      catch (e) {
          // AJAX is not supported
          alert('AJAX is not supported. Please upgrade your browser!');
      };
  };

}

function doChartMNrewards() {
  var xhr = new XMLHttpRequest();
  var url = 'https://api.egem.io/dailymnrewards';
  xhr.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {

          var response = JSON.parse(this.responseText);

          var ctx = document.getElementById('mnrewTime');

          var labelArray = [];

          for (var i = 0; i < response.length; i++) {
            labelArray.push(i);
          }

          var labelData = labelArray;
          var priceData = response;

          var supplyChart = new Chart(ctx, {
            type: 'line',
            data: {
              labels: labelData,
              datasets: [{
                  data: priceData,
                  label: "Daily System-wide QN Rewards",
                  borderColor: "#E4EBFD",
                  fill: true
                }
              ]
            },
            options: {
              title: {
                display: true,
                text: 'Quarrynode system balance over time.'
              }
            }
          });

      }
  };
  xhr.open("GET", url, true);
  xhr.send();

  try {
      // Compliant browsers
      xhr = new XMLHttpRequest();
  }

  catch (e) {
      try {
          // IE7+
          xhr = new ActiveXObject("Msxml2.XMLHTTP");
      }
      catch (e) {
          // AJAX is not supported
          alert('AJAX is not supported. Please upgrade your browser!');
      };
  };

}

function doChartT1() {
  var xhr = new XMLHttpRequest();
  var url = 'https://api.egem.io/dailyt1';
  xhr.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {

          var response = JSON.parse(this.responseText);

          var ctx = document.getElementById('t1Time');

          var labelArray = [];

          for (var i = 0; i < response.length; i++) {
            labelArray.push(i);
          }

          var labelData = labelArray;
          var priceData = response;

          var supplyChart = new Chart(ctx, {
            type: 'line',
            data: {
              labels: labelData,
              datasets: [{
                  data: priceData,
                  label: "Daily Tier One Pay",
                  borderColor: "#8987DF",
                  fill: true
                }
              ]
            },
            options: {
              title: {
                display: true,
                text: 'Payment data over time.'
              }
            }
          });

      }
  };
  xhr.open("GET", url, true);
  xhr.send();

  try {
      // Compliant browsers
      xhr = new XMLHttpRequest();
  }

  catch (e) {
      try {
          // IE7+
          xhr = new ActiveXObject("Msxml2.XMLHTTP");
      }
      catch (e) {
          // AJAX is not supported
          alert('AJAX is not supported. Please upgrade your browser!');
      };
  };

}

function doChartT2() {
  var xhr = new XMLHttpRequest();
  var url = 'https://api.egem.io/dailyt2';
  xhr.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {

          var response = JSON.parse(this.responseText);

          var ctx = document.getElementById('t2Time');

          var labelArray = [];

          for (var i = 0; i < response.length; i++) {
            labelArray.push(i);
          }

          var labelData = labelArray;
          var priceData = response;

          var supplyChart = new Chart(ctx, {
            type: 'line',
            data: {
              labels: labelData,
              datasets: [{
                  data: priceData,
                  label: "Daily Tier Two Pay",
                  borderColor: "#D0F7FF",
                  fill: true
                }
              ]
            },
            options: {
              title: {
                display: true,
                text: 'Payment data over time.'
              }
            }
          });

      }
  };
  xhr.open("GET", url, true);
  xhr.send();

  try {
      // Compliant browsers
      xhr = new XMLHttpRequest();
  }

  catch (e) {
      try {
          // IE7+
          xhr = new ActiveXObject("Msxml2.XMLHTTP");
      }
      catch (e) {
          // AJAX is not supported
          alert('AJAX is not supported. Please upgrade your browser!');
      };
  };

}
